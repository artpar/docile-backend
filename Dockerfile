FROM scratch
ADD ca-certificates.crt /etc/ssl/certs/
ADD aws_config /.kube/config
ADD main /
CMD ["/main"]
