package deployment

import "bitbucket.org/artpar/docile-backend/utils"

func (dm *DeploymentManager) CreateVolumes(instance Instance) {

	volumes := ToKubernetesVolume(&instance)
	volumes, err := dm.ClientSet.CreateVolume(volumes)
	utils.InfoError(err, "Error while creating volumes for deployment: %v", instance.Name)

}
