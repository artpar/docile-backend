package deployment

import (
	"bitbucket.org/artpar/docile-backend/utils"
	"k8s.io/api/core/v1"
	"log"
)

func (dm *DeploymentManager) StartServiceToImageInCluster(instance Instance) []*v1.Service {
	log.Printf("Start a new service to instance: %v", instance.Image)
	newServiceInstance := ToKubernetesService(instance)
	newService, err := dm.ClientSet.CreateService(newServiceInstance)
	if utils.InfoError(err, "Failed to create new pod") {
		return newService
	}

	ingresses := ToKubernetesIngress(instance)
	dm.ClientSet.CreateIngress(ingresses)

	_, err = dm.Db.Exec("UPDATE deployment SET domain_name = ? WHERE id = ?", "nothing", instance.Id)
	utils.InfoError(err, "Failed to update deployment id and status for [%v]", instance.Id)

	return newService
}
