package deployment

import (
	"bitbucket.org/artpar/docile-backend/utils"
	"encoding/json"
	"github.com/digitalocean/godo"
	"log"
)

func (dm *DeploymentManager) StartImageInCluster(instance Instance) {
	log.Printf("Start a new instance: %v", *instance.Name)

	newPod, clusterId, err := dm.ClientSet.CreateDeployment(ToKubernetesDeployment(&instance))
	if utils.InfoError(err, "Failed to create new pod") {
		return
	}
	log.Printf("Droplet information: %v", newPod)

	instance.ClusterStatus.DeploymentStatus = "booting"
	bstr, err := json.Marshal(instance.ClusterStatus)
	utils.InfoError(err, "Failed to marshal status")
	instance.ClusterStatusString = string(bstr)

	_, err = dm.Db.Exec("UPDATE deployment SET cluster_instance_id = ?, cluster_status = ? WHERE id = ?", clusterId, instance.ClusterStatusString, instance.Id)
	utils.InfoError(err, "Failed to update deployment id and status for [%v]", clusterId)

	dm.StartServiceToImageInCluster(instance)

}

func (manager *DeploymentManager) GetDropletsByDeploymentId(tag string) ([]godo.Droplet, error) {
	return manager.ClientSet.GetInstanceByTag(tag)
}

func (dm *DeploymentManager) FetchDeploymentUpdates(instance Instance) {
	droplets, err := dm.ClientSet.GetInstanceByTag(*instance.ClusterInstanceId)
	if utils.InfoError(err, "Failed to get instance by tag") {
		return
	}
	firstContainer := droplets[0]

	log.Printf("Droplet status: %v", firstContainer)
	if firstContainer.Status == "active" {
		dm.UpdateInstanceStatus(&instance, "deployment", "booted")
	}

}
