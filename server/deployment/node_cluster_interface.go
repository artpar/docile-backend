package deployment

import (
	"k8s.io/api/core/v1"
	"k8s.io/api/extensions/v1beta1"
	"github.com/digitalocean/godo"
	"k8s.io/api/apps/v1beta2"
)

type NodeCluster interface {
	ListRunningInstances() ([]godo.Droplet, error)
	CreateService(service []*v1.Service) ([]*v1.Service, error)
	CreateIngress(ingress []*v1beta1.Ingress) ([]*v1beta1.Ingress, error)
	CreateVolume(ingress []*v1.PersistentVolume) ([]*v1.PersistentVolume, error)
	CreateDeployment(deployment []*v1beta2.Deployment) ([]*godo.Droplet, string, error)
	MountVolumeToInstance(volume*v1.PersistentVolume, droplet godo.Droplet) error
	GetInstanceByTag(instanceId string) ([]godo.Droplet, error)
}
