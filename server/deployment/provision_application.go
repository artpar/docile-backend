package deployment

import (
	"bitbucket.org/artpar/docile-backend/utils"
	"fmt"
	"github.com/digitalocean/godo"
	"k8s.io/api/core/v1"
	"log"
	"strings"
)

func (dm *DeploymentManager) CreatePodsInMachine(instance Instance) {

	droplets, err := dm.ClientSet.GetInstanceByTag(*instance.ClusterInstanceId)

	if len(droplets) == 0 || utils.InfoError(err, "Failed to get instance by cluster reference id") {
		dm.UpdateInstanceStatus(&instance, "deployment", "missing")
		return
	}

	if utils.InfoError(err, "Failed to get instance by cluster reference id") {
		dm.UpdateInstanceStatus(&instance, "deployment", "missing")
		return
	}

	firstContainer := droplets[0]

	connection, err := NewRemoteConnection(firstContainer.Networks.V4[0].IPAddress, "root", utils.SshKeyPrivate)
	if utils.InfoError(err, "Failed to ssh into the instance") {
		dm.UpdateInstanceStatus(&instance, "deployment", "booted")
		return
	}
	err = connection.ExecuteSshCommand("apt-get update")
	if utils.InfoError(err, "Failed to execute ssh command: apt-get update") {
		dm.UpdateInstanceStatus(&instance, "deployment", "booted")
		return
	}
	err = connection.ExecuteSshCommand("apt-get install -y docker.io")
	if utils.InfoError(err, "Failed to execute ssh commandL apt-get install docker") {
		dm.UpdateInstanceStatus(&instance, "deployment", "booted")
		return
	}
	connection.Close()

	deployments := ToKubernetesDeployment(&instance)

	volumes := ToKubernetesVolume(&instance)
	volumeMap := make(map[string]*v1.PersistentVolume)
	for _, vol := range volumes {
		volumeMap[vol.Name] = vol
	}

	for _, deployment := range deployments {

		containers := deployment.Spec.Template.Spec.Containers
		volumesForContainer := deployment.Spec.Template.Spec.Volumes
		volumesForContainerMap := make(map[string]string)
		for _, vol := range volumesForContainer {
			volumesForContainerMap[vol.Name] = vol.PersistentVolumeClaim.ClaimName
		}

	ContainerLoop:
		for _, container := range containers {

			for _, volumeMount := range container.VolumeMounts {
				persistentVolume := volumeMap[volumesForContainerMap[volumeMount.Name]]

				err = dm.MountVolumeToContainer(persistentVolume, firstContainer)
				if utils.InfoError(err, "failed to mount volume to container") {
					break ContainerLoop
				}
			}

			connection, err = NewRemoteConnection(firstContainer.Networks.V4[0].IPAddress, "root", utils.SshKeyPrivate)
			if utils.InfoError(err, "Failed to connect to the target machine by ssh") {
				continue
			}

			log.Printf("Spawn a container for image [%v]", container.Image)

			args := make([]string, 0)

			for _, port := range container.Ports {

				if port.HostPort == 0 {
					port.HostPort = port.ContainerPort
				}

				argString := fmt.Sprintf("-p %d:%d", port.HostPort, port.ContainerPort)
				args = append(args, argString)
			}

			for _, volumeMount := range container.VolumeMounts {
				persistentVolume := volumeMap[volumesForContainerMap[volumeMount.Name]]
				mountPath := dm.MountVolumeToInstance(persistentVolume, connection)
				argString := fmt.Sprintf("-v %v:%v", mountPath, volumeMount.MountPath)
				args = append(args, argString)
			}
			args = append(args, "-d")
			args = append(args, container.Image)
			cmd := fmt.Sprintf("docker run %v", strings.Join(args, " "))
			log.Printf("Docker: %v", cmd)
			connection.ExecuteSshCommand(cmd)
		}
	}
	dm.UpdateInstanceStatus(&instance, "deployment", "complete")
}

func (manager *DeploymentManager) MountVolumeToInstance(volume *v1.PersistentVolume, connection *RemoteConnection) string {
	log.Printf("Mount volume")
	if volume == nil {
		log.Panic("Volumn is nil")
	}

	log.Printf("Volume annotations: %v", volume.Annotations)
	ref := volume.Annotations["reference_id"]
	log.Printf("Mount volume [%v] to instance", ref)
	mountPath := fmt.Sprintf("/mnt/volume_%s-part1", ref)
	connection.ExecuteSshCommand(fmt.Sprintf("yes | parted /dev/disk/by-id/scsi-0DO_Volume_%s mklabel gpt", ref))
	connection.ExecuteSshCommand(fmt.Sprintf("yes | parted -a opt /dev/disk/by-id/scsi-0DO_Volume_%s mkpart primary ext4 0%% 100%%", ref))
	connection.ExecuteSshCommand(fmt.Sprintf("yes | mkfs.ext4 /dev/disk/by-id/scsi-0DO_Volume_%s-part1", ref))
	connection.ExecuteSshCommand(fmt.Sprintf("mkdir -p %s", mountPath))
	connection.ExecuteSshCommand(fmt.Sprintf("echo '/dev/disk/by-id/scsi-0DO_Volume_%s-part1 %s ext4 defaults,nofail,discard 0 2' | sudo tee -a /etc/fstab", ref, mountPath))
	connection.ExecuteSshCommand("mount -a")

	return mountPath

}
func (manager *DeploymentManager) MountVolumeToContainer(volume *v1.PersistentVolume, droplet godo.Droplet) error {

	if volume == nil || droplet.ID == 0 {
		log.Panic("volume or droplet is nill")
	}

	log.Printf("Mount a volume to a container")
	return manager.ClientSet.MountVolumeToInstance(volume, droplet)
}
