package deployment

import (
	"bitbucket.org/artpar/docile-backend/utils"
	"golang.org/x/crypto/ssh"
	"io"
	"log"
)

type RemoteConnection struct {
	user       string
	address    string
	privateKey string
	session    *ssh.Session
	writer     io.WriteCloser
	reader     io.Reader
}

func (rc *RemoteConnection) Close() error {
	err := rc.session.Close()
	return err
}

func NewRemoteConnection(address, user, privateKey string) (*RemoteConnection, error) {

	sshSession, err := ConnectClient(address, user, privateKey)
	if err != nil {
		return nil, err
	}

	modes := ssh.TerminalModes{
		ssh.ECHO:          0,     // disable echoing
		ssh.TTY_OP_ISPEED: 14400, // input speed = 14.4kbaud
		ssh.TTY_OP_OSPEED: 14400, // output speed = 14.4kbaud
	}

	if err = sshSession.RequestPty("xterm", 80, 80, modes); err != nil {
		utils.InfoError(err, "Failed to get xterm for ssh")
		return nil, err
	}

	w, err := sshSession.StdinPipe()
	if err != nil {
		panic(err)
	}
	r, err := sshSession.StdoutPipe()
	if err != nil {
		panic(err)
	}

	if err := sshSession.Start("/bin/sh"); err != nil {
		log.Fatal(err)
	}

	connection := RemoteConnection{
		user:       user,
		address:    address,
		privateKey: privateKey,
		session:    sshSession,
		writer:     w,
		reader:     r,
	}
	err = connection.ExecuteSshCommand("PS1=\"" + escapePrompt + "\"")
	return &connection, err
}

func ConnectClient(address, user, privateKey string) (*ssh.Session, error) {

	// privateKey could be read from a file, or retrieved from another storage
	// source, such as the Secret Service / GNOME Keyring
	key, err := ssh.ParsePrivateKey([]byte(privateKey))
	if err != nil {
		return nil, err
	}
	// Authentication
	log.Printf("Use ssh key: %v", key)
	config := &ssh.ClientConfig{
		User: user,
		Auth: []ssh.AuthMethod{
			ssh.PublicKeys(key),
		},
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
		//alternatively, you could use a password
		/*
			Auth: []ssh.AuthMethod{
				ssh.Password("PASSWORD"),
			},
		*/
	}
	// Connect
	client, err := ssh.Dial("tcp", address+":22", config)
	if err != nil {
		return nil, err
	}
	// Create a session. It is one session per command.
	session1, err := client.NewSession()
	// you can also pass what gets input to the stdin, allowing you to pipe
	// content from client to server
	//      session.Stdin = bytes.NewBufferString("My input")

	return session1, err

}

func isMatch(bytes []byte, t int, matchingBytes []byte) bool {
	if t >= len(matchingBytes) {
		for i := 0; i < len(matchingBytes); i++ {
			//log.Printf("[%v] != [%v]", bytes[t-len(matchingBytes)+i], matchingBytes[i])
			if bytes[t-len(matchingBytes)+i] != matchingBytes[i] {
				return false
			}
		}
		log.Printf("Found match for %v", matchingBytes)
		return true
	}
	return false
}

func write(w io.WriteCloser, command string) error {
	_, err := w.Write([]byte(command + "\n"))
	return err
}

func readUntil(r io.Reader, matchingByte []byte) (string, error) {
	var buf [64 * 1024]byte
	var t int
	for {
		n, err := r.Read(buf[t:])
		if err != nil || n == 0 {
			stringResult := string(buf[:t])
			return stringResult, err
		}
		t += n
		if isMatch(buf[:t], t, matchingByte) {
			stringResult := string(buf[:t-len(matchingByte)])
			return stringResult, nil
		}
	}
}

var escapePrompt = "@@@"
var escapePromptBytes = []byte(escapePrompt)

//e.g. output, err := remoteRun("root", "MY_IP", "PRIVATE_KEY", "ls")
func (rc *RemoteConnection) ExecuteSshCommand(cmd string) error {

	log.Printf("Execute ssh command: %v", cmd)
	// Finally, run the command

	err := write(rc.writer, cmd)
	if utils.InfoError(err, "Failed to write command [%v] to ssh session") {
		return err
	}
	if utils.InfoError(err, "Failed waiting for response") {
		return err
	}

	respo, err := readUntil(rc.reader, escapePromptBytes)

	log.Printf("Response: %v :: %v", respo, err)
	return err

}
