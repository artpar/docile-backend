package deployment

import (
	"bitbucket.org/artpar/docile-backend/utils"
	"encoding/json"
	"fmt"
	"log"
	"time"
)

func (dm *DeploymentManager) Ping() {
	log.Printf("Pong")
}

type DeploymentManager struct {
	ApplicationContext
}

type ClusterStatus struct {
	DeploymentStatus string
	VolumeStatus     string
}

type Instance struct {
	Id                      uint64
	Image                   string  `db:"image"`
	ClusterInstanceId       *string `db:"cluster_instance_id"`
	Namespace               *string
	DeploymentConfiguration *string    `db:"deployment_configuration"`
	ServiceConfiguration    *string    `db:"service_configuration"`
	ValidUntil              *time.Time `db:"valid_until"`
	ReferenceId             string     `db:"reference_id"`
	PackageName             *string    `db:"package_name"`
	ClusterStatus           ClusterStatus
	ClusterStatusString     string `db:"cluster_status"`
	Cost                    int
	Title                   *string
	DomainName              *string `db:"domain_name"`
	Name                    *string
	Port                    int
}

func (dm *DeploymentManager) GetDeploymentClusterInstanceId(clusterId string) (Instance, error) {
	return Instance{}, nil
}

func (dm *DeploymentManager) GetAllDeployments() ([]Instance, error) {
	deployments := make([]Instance, 0)
	s := "select inst.id, " +
		"inst.valid_until, " +
		"inst.cluster_instance_id, " +
		"inst.namespace, " +
		"inst.name, " +
		"pa.domain_name, " +
		"pa.package_name, " +
		"pa.cost, " +
		"pa.domain_name, " +
		"inst.cluster_status, " +
		"im.image, " +
		"im.port, " +
		"inst.deployment_configuration as deployment_configuration," +
		"im.title " +
		"from deployment inst " +
		"join package pa on pa.id = inst.package_id " +
		"join image im on im.id = pa.image_id"
	log.Printf("Query: %s", s)
	if dm.Db == nil {
		log.Printf("db is null")
	}
	rows, err := dm.Db.Queryx(s)
	utils.InfoError(err, "Failed to select deployments")
	if err != nil {
		return deployments, err
	}

	for rows.Next() {
		inst := Instance{}
		err = rows.StructScan(&inst)
		utils.InfoError(err, "Failed to scan deployment struct")
		err = json.Unmarshal([]byte(inst.ClusterStatusString), &inst.ClusterStatus)
		utils.InfoError(err, "Failed to unmarshal cluster status")
		deployments = append(deployments, inst)
	}
	rows.Close()

	return deployments, nil
}

func (dm *DeploymentManager) CheckNewDeployments() {

	log.Printf("Begin, check all deployments")
	deployments, err := dm.GetAllDeployments()
	if utils.InfoError(err, "Failed to get deployments") {
		return
	}
	log.Printf("Found %d deployments", len(deployments))

	knownContainers := make(map[string]bool)

	for _, deployment := range deployments {

		if deployment.DeploymentConfiguration == nil {
			log.Printf("No deployment configuration for deployment %v", deployment.Id)
			continue
		}

		CheckAndUpdateDeploymentConfig(&deployment)

		dm.UpdateInstanceDeploymentConfiguration(&deployment)

		switch deployment.ClusterStatus.DeploymentStatus {

		case "booted":
			knownContainers[*deployment.ClusterInstanceId] = true
			log.Printf("Found a running container for [%v]: %v", deployment.ClusterInstanceId, deployment.ClusterStatus)
			dm.UpdateInstanceStatus(&deployment, "deployment", "deploying")
			go dm.CreatePodsInMachine(deployment)

		case "booting":
			go dm.FetchDeploymentUpdates(deployment)

		case "missing":
			log.Printf("Deployed instance is missing, creating again")
			go dm.StartImageInCluster(deployment)
		case "initiated":
			fallthrough
		case "":
			log.Printf("Found new deployment to do: %v", deployment)
			go dm.StartImageInCluster(deployment)
			go dm.CreateVolumes(deployment)
		default:
			log.Printf("Unknown deployment status: %v", deployment)

		}

	}
}
func CheckAndUpdateDeploymentConfig(instance *Instance) {

	volumes := ToKubernetesVolume(instance)

	for i, vol := range volumes {

		if vol.Annotations == nil {
			vol.Annotations = make(map[string]string)
		}

		_, ok := vol.Annotations["reference_id"]
		if ok {
			continue
		}

		vol.Annotations["reference_id"] = "vol-" + utils.NewV4Uuid()
		log.Printf("Generate new reference id for volume [%v]: %v", vol.Name, vol.Annotations["reference_id"])
		volumes[i] = vol
	}

	SetKubernetesVolume(instance, volumes)

}

func (dm *DeploymentManager) UpdateInstanceDeploymentConfiguration(instance *Instance) error {
	_, err := dm.Db.Exec("update deployment set deployment_configuration = ? where id = ?", instance.DeploymentConfiguration, instance.Id)
	return err
}

func (dm *DeploymentManager) UpdateInstanceStatus(instance *Instance, key string, value string) error {

	switch key {
	case "deployment":
		instance.ClusterStatus.DeploymentStatus = value
	default:
		return fmt.Errorf("invalid key [%v]", key)
	}
	var err error
	bstr, err := json.Marshal(instance.ClusterStatus)

	if utils.InfoError(err, "Failed to marshal cluster status to string") {
		return err
	}
	instance.ClusterStatusString = string(bstr)

	_, err = dm.Db.Exec("update deployment set cluster_status = ? where cluster_instance_id = ?", instance.ClusterStatusString, instance.ClusterInstanceId)
	utils.InfoError(err, "Failed to update deployment status")
	return err

}
