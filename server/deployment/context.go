package deployment

import "github.com/jmoiron/sqlx"

type ApplicationContext struct {
	ClientSet NodeCluster
	Namespace string
	Db        *sqlx.DB
}

func BuildAppContext(clientset NodeCluster, db *sqlx.DB) ApplicationContext {

	appContext := ApplicationContext{}
	appContext.ClientSet = clientset
	appContext.Db = db
	return appContext
}
