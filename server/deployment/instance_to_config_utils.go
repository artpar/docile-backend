package deployment

import (
	"bitbucket.org/artpar/docile-backend/utils"
	"encoding/json"
	"fmt"
	"k8s.io/api/apps/v1beta2"
	v12 "k8s.io/api/core/v1"
	"k8s.io/api/extensions/v1beta1"
)

func ToKubernetesDeployment(instance *Instance) []*v1beta2.Deployment {

	config := make(map[string]interface{}, 0)

	response := make([]*v1beta2.Deployment, 0)
	err := json.Unmarshal([]byte(*instance.DeploymentConfiguration), &config)
	if utils.InfoError(err, "Failed to marshal deployment map to json 1") {
		return response
	}

	items, ok := config["items"].([]interface{})
	if !ok {
		utils.InfoError(fmt.Errorf("Failed to cast item as array 2"))
		return response
	}

	for i, itemInterface := range items {
		item, ok := itemInterface.(map[string]interface{})
		if !ok {
			utils.InfoError(fmt.Errorf("invalid deployment item at index %d", i))
			continue
		}
		if item["kind"].(string) == "Deployment" {
			deploymentJson, err := json.Marshal(item)
			utils.InfoError(err, "Failed to marshal deployment map to json")
			var deployment v1beta2.Deployment
			err = json.Unmarshal(deploymentJson, &deployment)
			utils.InfoError(err, "Failed to convert json to deployment")
			response = append(response, &deployment)
		}

	}

	return response

}

func ToKubernetesService(instance Instance) []*v12.Service {

	config := make(map[string]interface{}, 0)

	response := make([]*v12.Service, 0)
	err := json.Unmarshal([]byte(*instance.DeploymentConfiguration), &config)
	if utils.InfoError(err, "Failed to marshal deployment map to json") {
		return response
	}

	items, ok := config["items"].([]interface{})
	if !ok {
		utils.InfoError(fmt.Errorf("Failed to cast item as array"))
		return response
	}

	for i, itemInterface := range items {
		item, ok := itemInterface.(map[string]interface{})
		if !ok {
			utils.InfoError(fmt.Errorf("invalid deployment item at index %d", i))
			continue
		}
		if item["kind"].(string) == "Service" {
			deploymentJson, err := json.Marshal(item)
			utils.InfoError(err, "Failed to marshal deployment map to json 3")
			var deployment v12.Service
			err = json.Unmarshal(deploymentJson, &deployment)
			utils.InfoError(err, "Failed to convert json to deployment")
			response = append(response, &deployment)
		}

	}

	return response

}

func ToKubernetesIngress(instance Instance) []*v1beta1.Ingress {

	config := make(map[string]interface{}, 0)

	response := make([]*v1beta1.Ingress, 0)
	err := json.Unmarshal([]byte(*instance.DeploymentConfiguration), &config)
	if utils.InfoError(err, "Failed to marshal deployment map to json 4") {
		return response
	}

	items, ok := config["items"].([]interface{})
	if !ok {
		utils.InfoError(fmt.Errorf("Failed to cast item as array"))
		return response
	}

	for i, itemInterface := range items {
		item, ok := itemInterface.(map[string]interface{})
		if !ok {
			utils.InfoError(fmt.Errorf("invalid deployment item at index %d", i))
			continue
		}
		if item["kind"].(string) == "Ingress" {
			deploymentJson, err := json.Marshal(item)
			utils.InfoError(err, "Failed to marshal deployment map to json 5")
			var deployment v1beta1.Ingress
			err = json.Unmarshal(deploymentJson, &deployment)
			utils.InfoError(err, "Failed to convert json to deployment")
			response = append(response, &deployment)
		}

	}

	return response

}

func ToKubernetesVolume(instance *Instance) []*v12.PersistentVolume {

	config := make(map[string]interface{}, 0)

	response := make([]*v12.PersistentVolume, 0)
	err := json.Unmarshal([]byte(*instance.DeploymentConfiguration), &config)
	if utils.InfoError(err, "Failed to marshal deployment map to json 6") {
		return response
	}

	items, ok := config["items"].([]interface{})
	if !ok {
		utils.InfoError(fmt.Errorf("Failed to cast item as array"))
		return response
	}

	for i, itemInterface := range items {
		item, ok := itemInterface.(map[string]interface{})
		if !ok {
			utils.InfoError(fmt.Errorf("invalid deployment item at index %d", i))
			continue
		}
		if item["kind"].(string) == "PersistentVolumeClaim" {
			deploymentJson, err := json.Marshal(item)
			utils.InfoError(err, "Failed to marshal deployment map to json 7")
			var deployment v12.PersistentVolume
			err = json.Unmarshal(deploymentJson, &deployment)
			utils.InfoError(err, "Failed to convert json to deployment")
			response = append(response, &deployment)
		}

	}
	return response
}

func SetKubernetesVolume(instance *Instance, vols []*v12.PersistentVolume) []*v12.PersistentVolume {

	config := make(map[string]interface{}, 0)

	response := make([]*v12.PersistentVolume, 0)
	err := json.Unmarshal([]byte(*instance.DeploymentConfiguration), &config)
	if utils.InfoError(err, "Failed to marshal deployment map to json 8") {
		return response
	}

	items, ok := config["items"].([]interface{})
	if !ok {
		utils.InfoError(fmt.Errorf("Failed to cast item as array"))
		return response
	}

	filteredItems := make([]interface{}, 0)

	for i, itemInterface := range items {
		item, ok := itemInterface.(map[string]interface{})
		if !ok {
			utils.InfoError(fmt.Errorf("invalid deployment item at index %d", i))
			continue
		}
		if item["kind"].(string) == "PersistentVolumeClaim" {
			continue
		}

		filteredItems = append(filteredItems, itemInterface)

	}

	for _, vol := range vols {
		filteredItems = append(filteredItems, vol)
	}

	config["items"] = filteredItems
	configBytes, err := json.Marshal(config)
	if !utils.InfoError(err, "Failed to marshal updated deployment configuration") {
		s := string(configBytes)
		instance.DeploymentConfiguration = &s
	}
	return response
}
