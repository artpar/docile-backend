package server

import (
	"bitbucket.org/artpar/docile-backend/digitalocean"
	"bitbucket.org/artpar/docile-backend/kubeface"
)

type Configuration struct {
	Port string
}

type ApplicationConfiguration struct {
	Kubernetes     kubeface.KubernetesConfig
	DigitalOcean   digitalocean.DigitalOceanConfig
	DatabaseConfig DatabaseConfig
	Configuration  Configuration
}

type DatabaseConfig struct {
	Type             string
	ConnectionString string
}
