package server

import (
	"bitbucket.org/artpar/docile-backend/digitalocean"
	"bitbucket.org/artpar/docile-backend/server/deployment"
	"bitbucket.org/artpar/docile-backend/utils"
	"github.com/gorilla/mux"
	"github.com/jasonlvhit/gocron"
	"github.com/jmoiron/sqlx"
	"github.com/justinas/alice"
	_ "github.com/lib/pq"
	"github.com/spf13/viper"
	"io"
	"log"
	"net/http"
	"os"
)

func init() {

	viper.SetConfigName("config")
	viper.AddConfigPath(".")
	viper.AddConfigPath("/etc/dockercell/")
	viper.SetConfigType("yaml")
	err := viper.ReadInConfig() // Find and read the config file
	utils.CheckErr(err, "Failed to read in dockercell config")

}

func Main() {
	log.Printf("start dockercell")

	applicationConfig := ApplicationConfiguration{}
	err := viper.Unmarshal(&applicationConfig)
	utils.CheckErr(err, "Failed to read configuration file")

	db, err := sqlx.Open(applicationConfig.DatabaseConfig.Type, applicationConfig.DatabaseConfig.ConnectionString)
	utils.CheckErr(err, "Failed to connect to database")
	defer db.Close()

	//clientset, err := kubeface.NewKubefaceManager(applicationConfig.Kubernetes.ConfigLocation)
	digiSet, err := digitalocean.NewDigitalOceanNodeCluster(applicationConfig.DigitalOcean)
	utils.CheckErr(err, "Failed to connect to the cluster")

	appContext := deployment.BuildAppContext(digiSet, db)
	r := mux.NewRouter()

	r.NotFoundHandler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusNotFound)
		io.WriteString(w, "Not found")
	})
	r.PathPrefix("/").Handler(http.FileServer(http.Dir("static/dist/")))
	r.Path("").Handler(http.FileServer(http.Dir("static/dist/")))

	stack := alice.New().Then(r)

	// Start the server
	port := os.Getenv("PORT")
	if len(port) == 0 {
		port = applicationConfig.Configuration.Port
	}

	dm := &deployment.DeploymentManager{
		ApplicationContext: appContext,
	}
	SetupCronRoutines(dm)

	log.Printf("Running on port http://localhost:%v", port)
	log.Println(http.ListenAndServe("127.0.0.1:"+port, stack))

	utils.CheckErr(err, "Failed to start server")
}

func SetupCronRoutines(dm *deployment.DeploymentManager) {
	gocron.Every(30).Seconds().Do(func() {
		dm.CheckNewDeployments()
	})
	gocron.Every(10).Seconds().Do(dm.Ping)

	gocron.Start()
	go dm.CheckNewDeployments()
}
