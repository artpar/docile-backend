module bitbucket.org/artpar/docile-backend

go 1.13

require (
	github.com/artpar/go.uuid v1.2.0
	github.com/digitalocean/godo v1.29.0
	github.com/go-sql-driver/mysql v1.4.1
	github.com/gorilla/mux v1.7.3
	github.com/imdario/mergo v0.3.8 // indirect
	github.com/jasonlvhit/gocron v0.0.0-20191228163020-98b59b546dee
	github.com/jmoiron/sqlx v1.2.0
	github.com/justinas/alice v1.2.0
	github.com/lib/pq v1.0.0
	github.com/spf13/viper v1.6.1
	golang.org/x/crypto v0.0.0-20191227163750-53104e6ec876
	golang.org/x/oauth2 v0.0.0-20191202225959-858c2ad4c8b6
	k8s.io/api v0.17.0
	k8s.io/client-go v0.17.0
	k8s.io/utils v0.0.0-20191218082557-f07c713de883 // indirect
)
