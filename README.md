# DockerCell

Standard golang project

# Setup

```bash
glide install  # dependencies
go build main.go # build and run
go run main.go
```