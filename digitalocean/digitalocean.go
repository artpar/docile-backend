package digitalocean

import (
	"bitbucket.org/artpar/docile-backend/utils"
	"context"
	"crypto/md5"
	"encoding/base64"
	"fmt"
	"github.com/artpar/go.uuid"
	"github.com/digitalocean/godo"
	"golang.org/x/oauth2"
	"k8s.io/api/apps/v1beta2"
	"k8s.io/api/core/v1"
	"k8s.io/api/extensions/v1beta1"
	"log"
	"strings"
)

type DigitalOceanConfig struct {
	AccessToken string
}

type DigitalOceanNodeCluster struct {
	accessToken string
	client      *godo.Client
	keyMap      map[string]godo.Key
}

func FingerPrint() string {
	h := md5.New()
	decoded, err := base64.StdEncoding.DecodeString(strings.Split(utils.SshKeyPublic, " ")[1])
	if utils.InfoError(err, "Failed to decode public key as base64") {
		return ""
	}
	h.Write(decoded)
	fp := fmt.Sprintf("%x", h.Sum(nil))
	fp = strings.Join(utils.ChunkString(fp, 2), ":")
	return fp
}

func (donc *DigitalOceanNodeCluster) CreateDeployment(deployment []*v1beta2.Deployment) ([]*godo.Droplet, string, error) {

	u, err := uuid.NewV4()
	if utils.InfoError(err, "Failed to generate new cluster id by uuid") {
		return nil, "", err
	}
	deploymentClusterId := u.String()
	log.Printf("Deploy to DO with a new cluster id: %v", deploymentClusterId)
	log.Printf("Number of deployments: %d", len(deployment))

	log.Printf("Key fingerprint: %v", FingerPrint())

	dropletCreateRequest := &godo.DropletCreateRequest{
		Name:   deploymentClusterId,
		Region: "blr1",
		Size:   "s-1vcpu-1gb",
		Image: godo.DropletCreateImage{
			Slug: "ubuntu-16-04-x64",
		},
		SSHKeys: []godo.DropletCreateSSHKey{
			{
				ID: donc.keyMap[FingerPrint()].ID,
			},
		},
		Backups:           false,
		IPv6:              false,
		PrivateNetworking: false,
		Monitoring:        true,
		UserData:          "",
		Volumes:           []godo.DropletCreateVolume{},
		Tags: []string{
			deploymentClusterId,
		},
	}
	droplet, response, err := donc.client.Droplets.Create(context.Background(), dropletCreateRequest)
	CheckDOResponse(response)

	return []*godo.Droplet{droplet}, deploymentClusterId, err
}
func CheckDOResponse(response *godo.Response) {
	log.Printf("Limit: %v", response.Limit)
	log.Printf("Reset: %v", response.Reset)
	log.Printf("Remai: %v", response.Remaining)
}

func (donc *DigitalOceanNodeCluster) CreateService(service []*v1.Service) ([]*v1.Service, error) {
	log.Printf("Create service request: %v", service)
	return nil, nil
}

func (donc *DigitalOceanNodeCluster) CreateIngress(ingress []*v1beta1.Ingress) ([]*v1beta1.Ingress, error) {
	log.Printf("Create ingress request: %v", ingress)
	return nil, nil
}

func (donc *DigitalOceanNodeCluster) MountVolumeToInstance(vol *v1.PersistentVolume, droplet godo.Droplet) error {
	log.Printf("Mount a volume to a instance request")

	listParams := &godo.ListVolumeParams{
		Region: "blr1",
		Name:   vol.Annotations["reference_id"],
	}
	vo, do, err := donc.client.Storage.ListVolumes(context.TODO(), listParams)
	CheckDOResponse(do)
	if err != nil {
		return err
	}
	if len(vo) == 0 {
		return fmt.Errorf("No such persistent volume found")
	}
	vo1 := vo[0]

	for _, i := range vo1.DropletIDs {
		if i == droplet.ID {
			return nil
		}
	}

	_, dor, err := donc.client.StorageActions.Attach(context.TODO(), vo1.ID, droplet.ID)
	CheckDOResponse(dor)

	return err
}

func (donc *DigitalOceanNodeCluster) CreateVolume(volumes []*v1.PersistentVolume) ([]*v1.PersistentVolume, error) {
	log.Printf("Create volumes request: %v", volumes)

	batchId := utils.NewV4Uuid()

	for i, val := range volumes {
		region, ok := val.Annotations["region"]
		if !ok || region == "" {
			region = "blr1"
		}
		volumeName := val.Annotations["reference_id"]
		volumeCapacity := val.Spec.Capacity
		size, ok := volumeCapacity.Memory().AsInt64()
		if !ok {
			return volumes, nil
		}
		if size == 0 {
			size = 1
		}
		log.Printf("Request memory by volume [%v]: %v", val.Name, size)
		volumeCreateRequest := &godo.VolumeCreateRequest{
			Region:        region,
			Name:          volumeName,
			Description:   batchId,
			SizeGigaBytes: size,
		}
		_, doRes, err := donc.client.Storage.CreateVolume(context.TODO(), volumeCreateRequest)
		utils.InfoError(err, "Failed to create volume")
		CheckDOResponse(doRes)
		volumes[i] = val
	}

	return volumes, nil
}

func (donc *DigitalOceanNodeCluster) GetInstanceByTag(tag string) ([]godo.Droplet, error) {

	ctx := context.Background()
	droplet, response, err := donc.client.Droplets.ListByTag(ctx, tag, nil)
	CheckDOResponse(response)
	if utils.InfoError(err, "Failed to get droplet by id") {
		return nil, err
	}
	return droplet, err
}

func (donc *DigitalOceanNodeCluster) ListRunningInstances() ([]godo.Droplet, error) {

	listOptions := &godo.ListOptions{
		Page:    1,
		PerPage: 100,
	}
	droplets, response, err := donc.client.Droplets.List(context.Background(), listOptions)
	CheckDOResponse(response)

	if utils.InfoError(err, "Failed to list droplets from do") {
		return nil, err
	}

	log.Printf("Access token: %v", donc.accessToken)
	return droplets, nil
}

func (cluster DigitalOceanNodeCluster) LoadSshKeysFromAccount() {

	keyListOptions := godo.ListOptions{
		Page:    1,
		PerPage: 100,
	}
	keys, godoResponse, err := cluster.client.Keys.List(context.TODO(), &keyListOptions)
	if utils.InfoError(err, "Failed to list keys from digital ocean") {
		return
	}
	CheckDOResponse(godoResponse)
	for _, key := range keys {
		cluster.keyMap[key.Fingerprint] = key
	}

}

func (cluster DigitalOceanNodeCluster) CheckSshKey() {

	key, res, err := cluster.client.Keys.GetByFingerprint(context.TODO(), FingerPrint())
	if !utils.InfoError(err, "Failed to find ssh key by fingerprint") {
		log.Printf("Ssh key by fingerprint [%v] already exists", FingerPrint())
	}
	CheckDOResponse(res)
	if key == nil {
		log.Printf("Create new ssh key in do [%v]", FingerPrint())
		keyCreateRequest := &godo.KeyCreateRequest{
			Name:      "root",
			PublicKey: utils.SshKeyPublic,
		}
		_, res, err = cluster.client.Keys.Create(context.TODO(), keyCreateRequest)
		utils.InfoError(err, "Failed to add key to digital ocean")
		CheckDOResponse(res)
	}

}

func (t *DigitalOceanConfig) Token() (*oauth2.Token, error) {
	token := &oauth2.Token{
		AccessToken: t.AccessToken,
	}
	return token, nil
}

func NewDigitalOceanNodeCluster(config DigitalOceanConfig) (*DigitalOceanNodeCluster, error) {

	oauthClient := oauth2.NewClient(context.Background(), &config)

	cluster := DigitalOceanNodeCluster{
		accessToken: config.AccessToken,
		client:      godo.NewClient(oauthClient),
		keyMap:      make(map[string]godo.Key),
	}
	cluster.LoadSshKeysFromAccount()
	cluster.CheckSshKey()
	return &cluster, nil
}
