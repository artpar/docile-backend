package utils

import (
	"github.com/artpar/go.uuid"
	"log"
)

func ChunkString(s string, chunkSize int) []string {
	var chunks []string
	runes := []rune(s)

	if len(runes) == 0 {
		return []string{s}
	}

	for i := 0; i < len(runes); i += chunkSize {
		nn := i + chunkSize
		if nn > len(runes) {
			nn = len(runes)
		}
		chunks = append(chunks, string(runes[i:nn]))
	}
	return chunks
}

const SshKeyPublic = `ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDGIV1HZ7VlqZLc2+HDmoYv31g5VIh0x/hhbIu1xAdt56RYRJLYfewVmxfy87s86NGuvvQTBR/PzX8E6WuWAIXnwgnJh+9JpnMC4Q9P318mj92QPXTfhIW0PjdPQFfDLXnrewmPToYDOLWSxkvo8JI2V7Ji15Jrmmxrd58TSM5PFXpN67juMr3Iq36LxmCCDjuVH3j2fSzDwxvt7rmecXQLtZOAYUcVHa9tm5DL3HLmQ/BkeGPcGBOE0I1udYH7UkyHTPZSNQZSGOWVM7RIomLIMyonFfdwvkp6ZFikKUHRl3CcepOoZzikgfKNF5g/jxAcoDMtY1+6/0zFR75a/UAb artpar@Halodoctests-MacBook-Pro.local`

const SshKeyPrivate = `-----BEGIN RSA PRIVATE KEY-----
MIIEpQIBAAKCAQEAxiFdR2e1ZamS3Nvhw5qGL99YOVSIdMf4YWyLtcQHbeekWESS
2H3sFZsX8vO7POjRrr70EwUfz81/BOlrlgCF58IJyYfvSaZzAuEPT99fJo/dkD10
34SFtD43T0BXwy1563sJj06GAzi1ksZL6PCSNleyYteSa5psa3efE0jOTxV6Teu4
7jK9yKt+i8Zggg47lR949n0sw8Mb7e65nnF0C7WTgGFHFR2vbZuQy9xy5kPwZHhj
3BgThNCNbnWB+1JMh0z2UjUGUhjllTO0SKJiyDMqJxX3cL5KemRYpClB0ZdwnHqT
qGc4pIHyjReYP48QHKAzLWNfuv9MxUe+Wv1AGwIDAQABAoIBAQCa+KvaVy8Wdp2t
7LrY4XvF5B9AsHMPTP5lTZ61rL/W4lG0zOZDObM4bxg7axLwIHcqBKgqXF62jlik
weLp8DzMuwQmwxGSfQQGo3zzj5cANZUq5jcNQb3G7scaUQyUWHS9kH44vkWqLWgw
hrQHXmwu30djcpRPX/kf0e/2CIN+bMDfQ1LORBS3Mz+HYz270n1pemm9N28uEC06
qo9au5Nvjb0JdkEMuBzEZonOpkLZrTXYl3mnDEOwYJtQLRggy7u3gD/Th46DyRLn
CJdraxdIpn3+k8blxDtju5t70yt7ibQ0rSlU/s2H5b/9XEGdCUMQLOvho6H1dsl5
PlfZM4zJAoGBAOU5u5gabbsK1N/+nBzgW4a2PCsmU1zc3OH6Ncr9FFjnU9KtE/en
C4Q807MqUlOTQOLbuHwrAJKdia7akjemyNTsY3klR8S78uyOptC/NbZkKLDfdX67
0cLPhtcn6FUuDO9GEGhC2L09FRnZGs6C/o0iJXe5aE3fmo3Ohg6+QJuXAoGBAN1F
0/AOsKAF0+ivVyzRiPM5/YBsuI9gV8UG8g8ZpU8k/sh+yUZn5cOhoTc7uwGx43pV
5gsbYW6IN/a4UolKb0ksnRflsyFWlgu5B718snY05fBlxF8RzsEwZ/rQS5M8n2ql
zuyn2hbeyqPrn/vV3raFFFGoAtzLdCxCLsFtpWAdAoGBAJ/QjJYTdwW9G4XAAh+v
3KAjVffPlLdd1TJIPFD1+cWyIqLzwMQdVs4vinmTvCpAtDB2eD05zgBqjZ+3C9J5
GfQ5i3DfKRZk441CN9O6PDyZwDTw1v4w/KISBjTjwfwBS0tXUgEe+fWWLKwEzcfF
VKwZbn32al5A04SixJxq0KipAoGAfuspmL1kVDVjikjgsOVzL0go/vvdlOfHEE1e
lIi6SD5JIgmPO6ImqWLRTRQmvN8jQq14qRBIf5UEvLCXLwaR9J/xb56ic9HteMXm
Cas31hObfJty2rElLaytR7EoWEjii9KVcSsKLioWg5WIi3PuLjUBJaFAGxNcDHLd
zLcur4UCgYEAk8orwU0oWX0yrz0EauGb1NDcN3UT3RAWSBOaB75+CwN04hA/Ffct
6hpZaA5OkdOZLqL8+7W6f17kh6nPn+870jzJNeRRtA9O1IWlHIl6bqeSKOcJTUYk
z1ghuVw8JR+dq176bpSO59YOH6kZrHVMmqezrwtFl5zVsv+iCVeBwZw=
-----END RSA PRIVATE KEY-----`

func InfoError(err error, args ...interface{}) bool {
	if err != nil {
		if len(args) > 0 {
			fm := args[0].(string) + ": %v"
			args = args[1:]
			args = append(args, err)
			log.Printf(fm, args...)
			return true
		} else {
			log.Printf("%v", err)
			return true
		}
	}
	return false
}

func CheckErr(e error, s ...interface{}) {

	if e != nil {
		format := s[0].(string)
		args := s[1:]
		args = append(args, e)
		log.Panicf(format+": %v", args...)
	}
}

func NewV4Uuid() string {
	u, _ := uuid.NewV4()
	return u.String()
}
