package kubeface

import (
	"k8s.io/api/core/v1"
	"k8s.io/api/extensions/v1beta1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/clientcmd"
)

func NewKubefaceManager(kubeconfig string) (*kubernetes.Clientset, error) {
	// use the current context in kubeconfig
	config, err := clientcmd.BuildConfigFromFlags("", kubeconfig)
	if err != nil {
		panic(err.Error())
	}

	// create the clientset
	clientset, err := kubernetes.NewForConfig(config)
	return clientset, nil

}

type KubernetesConfig struct {
	ConfigLocation string
	Namespace      string
}

type KubernetesNodeClusterWrapper struct {
	clientset *kubernetes.Clientset
}

func NewKubernetesNodeClusterWrapper(clientSet *kubernetes.Clientset) *KubernetesNodeClusterWrapper {
	return &KubernetesNodeClusterWrapper{
		clientset: clientSet,
	}
}

func (kncw *KubernetesNodeClusterWrapper) CreateDeployment(deployment *v1beta1.Deployment) (*v1beta1.Deployment, error) {
	return nil, nil
}

func (kncw *KubernetesNodeClusterWrapper) CreateIngress(deployment *v1beta1.Ingress) (*v1beta1.Ingress, error) {
	return nil, nil
}

func (kncw *KubernetesNodeClusterWrapper) CreateService(deployment *v1.Service) (*v1.Service, error) {
	return nil, nil
}

func (kncw *KubernetesNodeClusterWrapper) ListRunningInstances() ([]*v1beta1.Deployment, error) {
	return nil, nil
}
